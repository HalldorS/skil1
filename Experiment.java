package skil1;

//import edu.princeton.cs.algs4.*;
import edu.princeton.cs.algs4.QuickFindUF;
import edu.princeton.cs.algs4.QuickUnionUF;
import edu.princeton.cs.algs4.Stopwatch;
import edu.princeton.cs.introcs.*;

public class Experiment {

	public static double runExperiment(int N) {

		Connection[] C = RandomConnections.genConnections(N);
	
		QuickUnionUF uf = new QuickUnionUF(N);
//		WQUPC uf = new WQUPC(N);
//		QuickFindUF uf = new QuickFindUF(N);
		Stopwatch s = new Stopwatch();
		for (int i = 0; i < C.length; i++) {
			if (uf.connected(C[i].p(), C[i].q())) continue;
			uf.union(C[i].p(), C[i].q());
		}

		return s.elapsedTime()/C.length;
	}
	
	public static void runExp() {
		
	}

	public static void main(String[] args) {

		int T = 6;
		if (args.length > 0)
			T = Integer.parseInt(args[0]);

		double time = 0.0, prev = 1000.0, avg = 0.0;

		
		StdOut.printf("N\tAvg\tRatio\n");
		for (int N = 2000; N <= 64000; N *= 2) {
			avg = 0.0;
			for (int k = 0; k < T; k++) {
				time = runExperiment(N);
				avg += time;
			}

			avg = avg / T;

			StdOut.printf("%d\t%5.4f\t%5.4f\n", N, avg, avg / prev);
			prev = avg;
		}
	}

}
