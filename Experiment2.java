package skil1;

import edu.princeton.cs.algs4.Stopwatch;
import edu.princeton.cs.introcs.StdOut;

public class Experiment2 {
	public static double runExperiment(int N, int T, Connection[] C, IUnion uf) {
		double avrg = 0;
		
		for (int k = 0; k < T; k++) {	
			Stopwatch s = new Stopwatch();
			for (int i = 0; i < C.length; i++) {
				if (uf.connected(C[i].p(), C[i].q()))
					continue;
				uf.union(C[i].p(), C[i].q());
			}

			avrg += s.elapsedTime();
		}
		
		return avrg/T;
	}

	public static void main(String[] args) {
		int T = 20;
		StdOut.printf("N        \t   Heighted\t   WQUPC\t    WeightedQU\n");
		for (int N = 8000; N <= 2048000; N *= 2) {
			Connection[] C = RandomConnections.genConnections(N);

			StdOut.printf("%d    \t", N);
			HeightedQU huf = new HeightedQU(N);
			double time = runExperiment(N, T, C, huf);
			StdOut.printf("%f\t", time);

			WQUPC wuf = new WQUPC(N);
			time = runExperiment(N, T, C, wuf);
			StdOut.printf("%f\t", time);
			
			WeightedQU wqu = new WeightedQU(N);
			time = runExperiment(N, T, C, wqu);
			StdOut.printf("%f\n", time);
		}
	}
}
