package skil1;

public class HeightedQU implements IUnion {
	private int[] id; // id[i] = parent of i
	private int[] height; // height[i] = height of the in tree rooted at i
	private int count; // number of components

	// Create an empty union find data structure with N isolated sets.
	public HeightedQU(int N) {
		count = N;
		id = new int[N];
		height = new int[N];
		for (int i = 0; i < N; i++) {
			id[i] = i;
			height[i] = 0;
		}
	}

	// Return the number of disjoint sets.
	public int count() {
		return count;
	}

	// Return component identifier for component containing p
	public int find(int p) {
		while (p != id[p])
			p = id[p];
		return p;
	}

	// Are objects p and q in the same set?
	public boolean connected(int p, int q) {
		return find(p) == find(q);
	}

	// Replace sets containing p and q with their union.
	public void union(int p, int q) {
		int i = find(p);
		int j = find(q);
		if (i == j)
			return;

		// make smaller root point to larger one
		if (height[i] < height[j]) {
			id[i] = j;
			// height[j] += height[i];
		} else {
			id[j] = i;
			// height[i] += height[j];
			if (height[i] == height[j]) {
				height[i]++;
			}
		}
		count--;
	}

}
