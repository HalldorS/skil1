package skil1;

public interface IUnion {
	public boolean connected(int p, int q);
	public void union(int p, int q);
}
