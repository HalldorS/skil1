package skil1;

import edu.princeton.cs.algs4.Stopwatch;
import edu.princeton.cs.algs4.WeightedQuickUnionUF;
import edu.princeton.cs.introcs.*;

import java.util.*;

/**
 * @author Halldor og Bjorn
 * 
 */
public class RandomConnections {
	public static Connection[] genConnections(int N) {
		int q, p;

//		WQUPC uf = new WQUPC(N);
		WeightedQuickUnionUF uf = new WeightedQuickUnionUF(N);

		// Temp list
		ArrayList<Connection> list = new ArrayList<Connection>();

		// We don't stop until all N elements in the collection are connected
		// together
		while (uf.count() > 1) {
			q = StdRandom.uniform(0, N);
			p = StdRandom.uniform(0, N);

			// If q & p cannot be the same point.
			if (p == q)
				continue;

			Connection connection = new Connection(p, q);
			uf.union(p, q);
			list.add(connection);
		}

		// Final list
		Connection[] connections = new Connection[list.size()];
		list.toArray(connections);
		return connections;
	}

	public static Connection[] genGrid(int N) {
		int x, y, p, q;
//		WQUPC uf = new WQUPC(N * N);
		WeightedQuickUnionUF uf = new WeightedQuickUnionUF(N);

		// Temp list
		ArrayList<Connection> list = new ArrayList<Connection>();


		// We don't stop until all N elements in the collection are connected
		// together
		while (uf.count() > 1) {
			// Find some random point.
			x = (int) StdRandom.uniform(0, Math.sqrt(N));
			y = (int) StdRandom.uniform(0, Math.sqrt(N));

			p = offset(N, x, y);
			// Random point to connect to.
			q = 0;

			// Get random direction. 0 is left, 1 is right, 2 is up and 3 is
			// down.
			int direction = StdRandom.uniform(0, 3);

			// Test if the other point is legal and then set it.
			if (direction == 0 && (x - 1) >= 0) { // Point to left.
				q = offset(N, x - 1, y);
			} else if (direction == 1 && (x + 1) < N) { // Point to right.
				q = offset(N, x + 1, y);
			} else if (direction == 2 && (y - 1) >= 0) { // Point up.
				q = offset(N, x, y - 1);
			} else if (direction == 3 && (y + 1) < N) { // Point down.
				q = offset(N, x, y + 1);
			} else {
				// Illegal point q so continue to next iteration.
				continue;
			}
			
			if (uf.connected(p, q)) continue;

			// Create the connection.
			Connection connection = new Connection(p, q);
			uf.union(p, q);
			list.add(connection);
		}

		// Final list
		Connection[] connections = new Connection[list.size()];
		list.toArray(connections);
		return connections;
	}

	public static void printConnections(int N) {
//		Connection C[] = genConnections(N);
		 Connection C[] = genGrid(N);
		for (int i = 0; i < C.length; i++)
			StdOut.printf("%d: (%d, %d)\n", i, C[i].p(), C[i].q());
	}

	/**
	 * offset is the position of x,y in one dimension.
	 * 
	 * @param N
	 *            is the width/height
	 * @param row
	 *            is the y
	 * @param col
	 *            is the x
	 * @return the position in one dimension array.
	 */
	public static int offset(int N, int col, int row) {
		return row * N + col;
	}

	public static void main(String[] args) {
//		printConnections(600);
		Stopwatch sw = new Stopwatch();
		genGrid(200);
		StdOut.print(sw.elapsedTime());
	}
}
